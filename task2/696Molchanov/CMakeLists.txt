set(SRC_FILES
                ${PROJECT_SOURCE_DIR}/common/Application.cpp
                ${PROJECT_SOURCE_DIR}/common/DebugOutput.cpp
                ${PROJECT_SOURCE_DIR}/common/Camera.cpp
                ${PROJECT_SOURCE_DIR}/common/Mesh.cpp
                ${PROJECT_SOURCE_DIR}/common/ShaderProgram.cpp
                ${PROJECT_SOURCE_DIR}/common/Texture.cpp
                ${PROJECT_SOURCE_DIR}/common/Framebuffer.cpp
                main.cpp
		)

set(HEADER_FILES
                ${PROJECT_SOURCE_DIR}/common/Application.hpp
                ${PROJECT_SOURCE_DIR}/common/DebugOutput.h
                ${PROJECT_SOURCE_DIR}/common/Camera.hpp
                ${PROJECT_SOURCE_DIR}/common/LightInfo.hpp
                ${PROJECT_SOURCE_DIR}/common/Mesh.hpp
                ${PROJECT_SOURCE_DIR}/common/ShaderProgram.hpp
                ${PROJECT_SOURCE_DIR}/common/Texture.hpp
                ${PROJECT_SOURCE_DIR}/common/Framebuffer.hpp
                )

COPY_RESOURCE(task2shaders)
COPY_RESOURCE(shaders)


MAKE_OPENGL_TASK(696Molchanov 2 "${SRC_FILES}")

if (UNIX)
	target_link_libraries(696Molchanov2 stdc++fs)
endif()

add_dependencies(696Molchanov2 shaders)
add_dependencies(696Molchanov2 task2shaders)
